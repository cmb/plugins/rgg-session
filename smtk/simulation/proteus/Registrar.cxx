//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/proteus/Registrar.h"

#include "smtk/common/Paths.h"
#include "smtk/common/PythonInterpreter.h"

#include "smtk/session/rgg/Resource.h"

#include "smtk/operation/RegisterPythonOperations.h"

namespace smtk
{
namespace simulation
{
namespace proteus
{

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  Registrar::addModuleToPythonPath();

  smtk::operation::registerPythonOperations(operationManager, "rggsession.simulation.proteus.export_to_proteus_sn");

  smtk::operation::registerPythonOperations(operationManager, "rggsession.simulation.proteus.generate_cross_sections");
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->unregisterOperation("rggsession.simulation.proteus.export_to_proteus_sn");
  operationManager->unregisterOperation("rggsession.simulation.proteus.generate_cross_sections");
}

void Registrar::addModuleToPythonPath()
{
  std::string libDir = smtk::common::Paths::pathToLibraryContainingFunction(
    smtk::simulation::proteus::Registrar::addModuleToPythonPath);
  smtk::common::PythonInterpreter::instance().addPathToPluginModule("rggsession", libDir);
}
}
}
}
