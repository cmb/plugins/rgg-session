set(rggSrcs
  Assembly.cxx
  Core.cxx
  Duct.cxx
  Material.cxx
  Pin.cxx
  rggPhraseModel.cxx
  Registrar.cxx
  Resource.cxx
  Session.cxx
  json/jsonAssembly.cxx
  json/jsonAssyExportParameters.cxx
  json/jsonCore.cxx
  json/jsonCoreExportParameters.cxx
  json/jsonDuct.cxx
  json/jsonPin.cxx
  meshkit/AssyExportParameters.cxx
  meshkit/CoreExportParameters.cxx
  operators/OperationHelper.cxx
  operators/ReadRXFFileHelper.cxx
  )

set(rggHeaders
  Assembly.h
  Core.h
  Duct.h
  Material.h
  Pin.h
  rggPhraseModel.h
  RegisterStyleFunction.h
  Registrar.h
  Resource.h
  Session.h
  json/jsonAssembly.h
  json/jsonAssyExportParameters.h
  json/jsonCore.h
  json/jsonCoreExportParameters.h
  json/jsonDuct.h
  json/jsonPin.h
  meshkit/AssyExportParameters.h
  meshkit/CoreExportParameters.h
  operators/OperationHelper.h
  operators/ReadRXFFileHelper.h
  )

set(rggOperators
  AddMaterial
  CreateModel
  Delete
  EditAssembly
  EditCore
  EditDuct
  EditMaterial
  EditPin
  ExportInp
  Read
  ReadRXFFile
  RemoveMaterial
  Write
)
# Operators which have XML descriptions in separate files
# need to have it encoded as a string in a header.
foreach (operator ${rggOperators})
  smtk_encode_file("${CMAKE_CURRENT_SOURCE_DIR}/operators/${operator}.sbt"
  TARGET_OUTPUT targetName)
  list(APPEND rggSrcs operators/${operator}.cxx)
  list(APPEND rggHeaders operators/${operator}.h)
  list(APPEND _rggDependencies ${targetName})
endforeach()

if (RGG_ENABLE_PYTHON_WRAPPING)
  set(relativedir "rggsession/session")
  set(rggPySrcs
    operators/generate_mesh.py
    )

  set(rgg_pymodulefiles)
  set(rgg_pyxmlfiles)

  smtk_encode_file("${CMAKE_CURRENT_SOURCE_DIR}/operators/generate_mesh.sbt"
    PYTHON
    DESTINATION "${PROJECT_BINARY_DIR}/${relativedir}/generate_mesh_xml.py"
    NAME "description"
    HEADER_OUTPUT rgg_pyxmlfile
  )
  list(APPEND rgg_pyxmlfiles "${rgg_pyxmlfile}"
  )
endif()

add_library(smtkRGGSession SHARED ${rggSrcs} ${rgg_pyxmlfiles})
add_dependencies(smtkRGGSession ${_rggDependencies})

target_link_libraries(smtkRGGSession
  PUBLIC
    smtkCore
    Boost::boost
    Boost::filesystem
  PRIVATE
    ${__dependencies}
    vtkSMTKSourceExt # vtkCmbLayeredConeSource
    smtkPVServerExt
)

target_include_directories(smtkRGGSession PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
  PRIVATE
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/thirdparty>
)

generate_export_header(smtkRGGSession EXPORT_FILE_NAME Exports.h)


if ( ParaView_FOUND AND Qt5_FOUND AND TARGET smtkQtExt)
  add_subdirectory(qt)
endif()

if (RGG_ENABLE_PYTHON_WRAPPING)
  foreach(pyfile ${rggPySrcs})
    get_filename_component(filename ${pyfile} NAME)
    configure_file(
      "${CMAKE_CURRENT_SOURCE_DIR}/${pyfile}"
      "${CMAKE_BINARY_DIR}/${relativedir}/${filename}" @ONLY
      )
    list(APPEND rgg_pymodulefiles "${CMAKE_BINARY_DIR}/${relativedir}/${filename}")
  endforeach()

  file(RELATIVE_PATH relativedir ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
  install(
    FILES ${rgg_pymodulefiles}
    DESTINATION "${PYTHON_MODULEDIR}/${relativedir}"
    )

  add_subdirectory(pybind11)
endif()

smtk_get_kit_name(name dir_prefix)

# Install the header files
install(
  FILES
    ${rggHeaders}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS smtkRGGSession
  EXPORT  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})


if (ENABLE_TESTING)
  add_subdirectory(testing)
endif()
